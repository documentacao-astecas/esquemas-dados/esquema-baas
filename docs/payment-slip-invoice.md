# Payment_Slip_Invoice

Tabela responsável por armazenar os boletos de pagamento referentes a cobranças feitas pelos clientes.

## Descrição dos campos da tabela

  | Campo                   | Tipo             | Descrição                                                                                               |
  | :---------------------- | :--------------- | :------------------------------------------------------------------------------------------------------ |
  | `id`                    | **int4**         | Identificador da tabela (Primary Key).                                                                  |
  | `idaccount`             | **int4**         | Identificador da conta.                                                                                 |
  | `covenantnumber`        | **int4**         | Número de convênio.                                                                                     |
  | `issuerbanknumber`      | **varchar(50)**  | Número da banco emissor.                                                                                |
  | `idbanknumber`          | **varchar(50)**  | Identificador do banco destinatário.                                                                    |
  | `uniqueid`              | **varchar(50)**  | Identificador de um boleto de pagamento específico.                                                     |
  | `datedocument`          | **date**         | Data do pagamento (formato ISO 8601).                                                                   |
  | `beneficiary`           | **jsonb**        | Objeto contendo os [^^dados^^](#__tabbed_1_1) do beneficiário.                                          |
  | `cobeneficiary`         | **jsonb**        | Objeto contendo os [^^dados^^](#__tabbed_1_2) da empresa que realizou a venda responsável pela geração de boleto (sacador avalista). |
  | `paymentslip`           | **jsonb**        | Objeto contendo os [^^dados^^](#__tabbed_1_3) do boleto de pagamento.                                   |
  | `discount`              | **jsonb**        | Objeto contendo os [^^dados^^](#__tabbed_1_5) referentes aos descontos no boleto.                       |
  | `interest`              | **jsonb**        | Objeto contendo os [^^dados^^](#__tabbed_1_6) das taxas de juros no boleto.                             |
  | `others`                | **jsonb**        | Objeto contendo informações ([^^dados^^](#__tabbed_1_7)) complementares sobre o boleto.                 |
  | `payer`                 | **jsonb**        | Objeto contendo informações ([^^dados^^](#__tabbed_1_8)) sobre o pagador do boleto.                     |
  | `bankbranchnumber`      | **int4**         | Número da agência do banco responsável pela criação do boleto de pagamento.                             |
  | `banknumber`            | **int4**         | Número do banco responsável pela criação do boleto.                                                     |
  | `instructions`          | **varchar(100)** | Instruções de pagamento do boleto.                                                                      |
  | `status`                | **int4**         | Status do boleto de pagamento.                                                                          |
  | `barcode`               | **varchar(50)**  | Código de barras do boleto.                                                                             |
  | `barcodenumber`         | **varchar(50)**  | Número do código de barras (linha digitável).                                                           |
  | `type`                  | **varchar(10)**  | Tipo de boleto que foi gerado.                                                                          |
  | `created_at`            | **timestamp**    |                                                                                                         |
  | `updated_at`            | **timestamp**    |                                                                                                         |
  | `lancamento_id`         | **int8**         | Foreing Key da tabela [^^lancamentos^^](#) (banco astecas).                                             |
  | `fornecedor_cliente_id` | **int8**         | Foreing Key da tabela [^^fornecedores_clientes^^](#) (banco astecas).                                   |
  | `documento_id`          | **int8**         | Foreing Key da tabela [^^documentos^^](#) (banco astecas).                                              |

### Objetos da tabela - Payment_Slip_Invoice

???+ info "Dados"

    === "Beneficiary"
        * **`documentType`:** Tipo de documento do beneficiário.
        * **`name`:** Nome do beneficiário.
        * **`state`:** Unidade federativa do beneficiário.
        * **`address`:** Endereço do beneficiário.
        * **`numberAddress`:** Número relacionando ao endereço do beneficiário.
        * **`neighborhood`:** Bairro relacionado ao endereço do beneficiário.
        * **`city`:** Cidade do beneficiário.
        * **`zipCode`:** Código postal do beneficiário.

    === "Cobeneficiary"
        * **`documentType`:** Tipo de documento do co-beneficiário.
        * **`documentNumber`:** Número do documento.
        * **`name`:** Nome do co-beneficiário.

    === "PaymentSlip"
        * **`documentType`:** Tipo de documento, podendo ser: duplicata mercantil ou duplicata de serviço.
        * **`dueDate`:** Data de pagamento do boleto.
        * **`amount`:** Valor a ser pago.

    === "Fine"
        * **`code`:** Código da multa, podendo ser: "1" = não registrada, "2" = valor fixo e "3" = multa percentual.
        * **`date`:** Data da multa.
        * **`amount`:** Valor da multa.

    === "Discount"
        * **`code`:** Código do desconto, podedo ser: "1" = porcentagem até a data informada ou "2" = porcentagem sobre os dias antecipados.
        * **`date`:** Data do desconto.
        * **`amount`:** Valor do desconto.
  
    === "Interest"
        * **`code`:** Código da taxa de juros, podendo ser: "1" = valor fixo ou "2" = * porcentagem.
        * **`date`:** Data da taxa de juros.
        * **`amountPerDay`:** Valor diário.

    === "Others"
        * **`acceptance`:** Descreve se o boleto teve o aceite realizado pelo beneficiário.
        * **`typeAutomaticCancellation`:** Tipo de cancelamento automático do boleto, podendo ser: "01" = tem cancelamento automático ou "02" = não tem cancelamento automático.
        * **`deadlineAutomaticCancellation`:** Data limite para cancelamento automático do boleto após o dia de vencimento.
        * **`typeContestation`:** Protesto de título, podendo ser: "01" = tem contestação o "02" = não tem contestação.
        * **`contestationNumberOfDays`:** Dias de protesto de título após a data de vencimento.

    === "Payer"
        * **`documentType`:** Tipo de pagador, podendo ser: "F" = individual (pessoa física) ou "J" = companhia (pessoa jurídica).
        * **`documentNumber`:** Documento do pagador.
        * **`name`:** Nome do pagador.
        * **`tradeName`:** Nome comercial.
        * **`address`:** Endereço do pagador.
        * **`city`:** Cidade do pagador.
        * **`state`:** Unidade federativa do pagador.
        * **`zipCode`:** Código postal do pagador.
    
## Relacionamentos

  | Tabela                                   | Tipo            | Descrição                                                            |
  | :--------------------------------------- | :-------------- | :------------------------------------------------------------------- |
  | [^^`astecas.lancamentos`^^](#)           | **Foreing Key** | Os boletos gerados estão vinculados a um lançamento.                 |
  | [^^`astecas.fornecedores_clientes`^^](#) | **Foreing Key** | Cada lançamento possui um fornecedor_cliente.                        |
  | [^^`astecas.documentos`^^](#)            | **Foreing Key** | Refere-se aos documentos gerados e que são armazenados no Amazon S3. |

*[Amazon S3]: Amazon Simple Storage Service